package io.inkable.notes;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import io.inkable.notes.fitting.CubicBezier;
import io.inkable.notes.fitting.CurveFitting;
import io.inkable.notes.util.Vec2;

public class TestBezierCurve {

    @Test
    public void test() {
        List<Vec2> expectations = Arrays.asList(
                new Vec2(0, 0),
                new Vec2(0, 0.5f),
                new Vec2(1.1f, 1.4f),
                new Vec2(2.1f, 1.6f),
                new Vec2(3.2f, 1.1f),
                new Vec2(4, 0.2f),
                new Vec2(4, 0)
        );

        for (int i = 0; i < expectations.size(); i++)
            expectations.set(i, expectations.get(i).scale(10));

        List<CubicBezier> curves = CurveFitting.fitCurve(expectations, 10);
        for (CubicBezier curve : curves)
            System.out.println(curve.getError() + " " + Arrays.toString(curve.getControlPoints()));
    }
}
