package io.inkable.notes.fitting;

import io.inkable.notes.util.Vec2;

public class LinearBezier extends BezierCurve {

    public LinearBezier() {
        super(2);
    }

    public LinearBezier(Vec2 p0, Vec2 p2) {
        super(4);
        mControlPoints[0] = p0;
        mControlPoints[1] = p2;
    }

    @Override
    public Vec2 eval(double t) {
        // B(t) = (1-t)P0 + tP1

        double oneMinusT = 1f - t;
        double x = oneMinusT * mControlPoints[0].getX() + t * mControlPoints[1].getX();
        double y = oneMinusT * mControlPoints[0].getY() + t * mControlPoints[1].getY();
        return new Vec2(x, y);
    }

    @Override
    public BezierCurve getDerivation() {
        return null;
    }
}
