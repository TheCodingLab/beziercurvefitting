package io.inkable.notes.fitting;

import io.inkable.notes.util.Vec2;

public class CubicBezier extends BezierCurve {

    public CubicBezier() {
        super(4);
    }

    public CubicBezier(Vec2 p0, Vec2 p1, Vec2 p2, Vec2 p3) {
        super(4);
        mControlPoints[0] = p0;
        mControlPoints[1] = p1;
        mControlPoints[2] = p2;
        mControlPoints[3] = p3;
    }

    @Override
    public Vec2 eval(double t) {
        // B(t) = (1-t)^3P0 + 3t(1-t)^2P1 + 3t^2(1-t)P2 + t^3P3
        double x = b0(t) * mControlPoints[0].getX() + b1(t) * mControlPoints[1].getX() + b2(t) * mControlPoints[2].getX() + b3(t) * mControlPoints[3].getX();
        double y = b0(t) * mControlPoints[0].getY() + b1(t) * mControlPoints[1].getY() + b2(t) * mControlPoints[2].getY() + b3(t) * mControlPoints[3].getY();
        return new Vec2(x, y);
    }

    @Override
    public QuadraticBezier getDerivation() {
        // B'(t) = 3(1-t)^2(P1-P0) + 6t(1-t)(P2-P1) + 3t^2(P3-P2)
        // --> P0 = 3(P1-P0)
        // --> P1 = 3(P2-P1)
        // --> P3 = 3(P3-P2)

        Vec2 p0 = mControlPoints[1].sub(mControlPoints[0]).scale(3);
        Vec2 p1 = mControlPoints[2].sub(mControlPoints[1]).scale(3);
        Vec2 p2 = mControlPoints[3].sub(mControlPoints[2]).scale(3);
        return new QuadraticBezier(p0, p1, p2);
    }

    public static double b0(double t) {
        double tmp = 1f - t;
        return tmp * tmp * tmp;
    }

    public static double b1(double t) {
        double tmp = 1f - t;
        return 3 * t * tmp * tmp;
    }

    public static double b2(double t) {
        double tmp = 1f - t;
        return 3 * t * t * tmp;
    }

    public static double b3(double t) {
        return t * t * t;
    }
}
