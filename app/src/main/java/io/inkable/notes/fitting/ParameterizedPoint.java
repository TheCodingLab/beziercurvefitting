package io.inkable.notes.fitting;

import io.inkable.notes.util.Vec2;

public class ParameterizedPoint {

    private Vec2 mPosition;
    private double mParameter;

    public ParameterizedPoint(Vec2 position) {
        mPosition = position;
        mParameter = 0;
    }

    public ParameterizedPoint(Vec2 position, double parameter) {
        mPosition = position;
        mParameter = parameter;
    }

    public Vec2 getPosition() {
        return mPosition;
    }

    public void setPosition(Vec2 position) {
        mPosition = position;
    }

    public double getParameter() {
        return mParameter;
    }

    public void setParameter(double parameter) {
        mParameter = parameter;
    }
}
