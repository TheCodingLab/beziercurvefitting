package io.inkable.notes.fitting;

import io.inkable.notes.util.Vec2;

public class QuadraticBezier extends BezierCurve {

    public QuadraticBezier() {
        super(3);
    }

    public QuadraticBezier(Vec2 p0, Vec2 p1, Vec2 p2) {
        super(3);
        mControlPoints[0] = p0;
        mControlPoints[1] = p1;
        mControlPoints[2] = p2;
    }

    @Override
    public Vec2 eval(double t) {
        // B(t) = (1-t)^2P0 + 2t(1-t)P1 + t^2P2

        double oneMinusT = 1f - t;
        double a = oneMinusT * oneMinusT;
        double b = 2 * t * oneMinusT;
        double c = t * t;

        double x = a * mControlPoints[0].getX() + b * mControlPoints[1].getX() + c * mControlPoints[2].getX();
        double y = a * mControlPoints[0].getY() + b * mControlPoints[1].getY() + c * mControlPoints[2].getY();
        return new Vec2(x, y);
    }

    @Override
    public LinearBezier getDerivation() {
        // B'(t) = 2(1-t)(P1-P0) + 2t(P2-P1)
        // --> P0 = 2(P1-P0)
        // --> P1 = 2(P2-P1)

        Vec2 p0 = mControlPoints[1].sub(mControlPoints[0]).scale(2);
        Vec2 p1 = mControlPoints[2].sub(mControlPoints[1]).scale(2);
        return new LinearBezier(p0, p1);
    }
}
