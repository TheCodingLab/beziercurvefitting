package io.inkable.notes.fitting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

import io.inkable.notes.util.Pointer;
import io.inkable.notes.util.Vec2;

public class CurveFitting {

    public static final int MAX_ITERATIONS = 4;

    /**
     * @param points data points drawn by user
     * @param error  maximum error
     */
    public static List<CubicBezier> fitCurve(List<Vec2> points, double error) {
        List<ParameterizedPoint> parameterizedPoints = new ArrayList<>();
        for (Vec2 v : points)
            parameterizedPoints.add(new ParameterizedPoint(v));

        Vec2 hat1 = computeLeftTangent(parameterizedPoints); // Compute tangent at first control point (second control point must lay on this line)
        Vec2 hat2 = computeRightTangent(parameterizedPoints); // Compute tangent at last control point (third control point must lay on this line)
        return fitCubic(parameterizedPoints, hat1, hat2, error);
    }

    public static List<CubicBezier> fitCubic(List<ParameterizedPoint> points, Vec2 hat1, Vec2 hat2, double error) {
        double iterationError = error * error;

        if (points.size() == 2) {
            // Only two points present --> straight line
            // TODO Repleace with straight line instead of bezier curve
            double dist = points.get(0).getPosition().distance(points.get(1).getPosition()) / 3.0f; // TODO Why divide by 3 tho

            CubicBezier curve = new CubicBezier();
            curve.setControlPoint(0, points.get(0).getPosition());
            curve.setControlPoint(3, points.get(1).getPosition());
            curve.setControlPoint(1, curve.getControlPoint(0).add(hat1.scale(dist)));
            curve.setControlPoint(2, curve.getControlPoint(3).add(hat2.scale(dist)));
            return Collections.singletonList(curve);
        }

        chordLengthParameterize(points);
        CubicBezier curve = generateBezier(points, hat1, hat2);

        Pointer<Integer> indexWithMaxError = new Pointer<>();
        double maxError = computeMaxError(points, curve, indexWithMaxError);
        if (maxError < error)
            return Collections.singletonList(curve);

        if (maxError < iterationError) {
            for (int i = 0; i < MAX_ITERATIONS; i++) {
                reparameterize(points, curve);

                curve = generateBezier(points, hat1, hat2);
                maxError = computeMaxError(points, curve, indexWithMaxError);
                if (maxError < error)
                    return Collections.singletonList(curve);
            }
        }

        Vec2 hatCenter = computeCenterTangent(points, indexWithMaxError.get());
        List<CubicBezier> a = fitCubic(points.subList(0, indexWithMaxError.get()), hat1, hatCenter, error);
        hatCenter = hatCenter.negate(); // Smooth transition between the two bezier curves
        List<CubicBezier> b = fitCubic(points.subList(indexWithMaxError.get() - 1, points.size()), hatCenter, hat2, error);

        List<CubicBezier> result = new ArrayList<>();
        result.addAll(a);
        result.addAll(b);
        return result;
    }

    public static CubicBezier generateBezier(List<ParameterizedPoint> points, Vec2 hat1, Vec2 hat2) {
        List<Vec2[]> a = new ArrayList<>();

        for (int i = 0; i < points.size(); i++) {
            ParameterizedPoint point = points.get(i);
            Vec2 v1 = hat1.scale(CubicBezier.b1(point.getParameter()));
            Vec2 v2 = hat2.scale(CubicBezier.b2(point.getParameter()));
            a.add(new Vec2[]{v1, v2});
        }

        double[][] cMatrix = new double[2][2];
        double[] xMatrix = new double[2];

        final Vec2 first = points.get(0).getPosition();
        final Vec2 last = points.get(points.size() - 1).getPosition();

        for (int i = 0; i < points.size(); i++) {
            Vec2[] aPoints = a.get(i);
            cMatrix[0][0] += aPoints[0].dot(aPoints[0]);
            cMatrix[0][1] += aPoints[0].dot(aPoints[1]);
            cMatrix[1][0] = cMatrix[0][1];
            cMatrix[1][1] += aPoints[1].dot(aPoints[1]);

            ParameterizedPoint point = points.get(i);
            Vec2 p = point.getPosition();

            Vec2 tmp0 = first.scale(CubicBezier.b0(point.getParameter()));
            Vec2 tmp1 = first.scale(CubicBezier.b1(point.getParameter()));
            Vec2 tmp2 = last.scale(CubicBezier.b2(point.getParameter()));
            Vec2 tmp3 = last.scale(CubicBezier.b3(point.getParameter()));

            Vec2 tmp = p.sub(tmp0.add(tmp1).add(tmp2).add(tmp3));

            xMatrix[0] += aPoints[0].dot(tmp);
            xMatrix[1] += aPoints[1].dot(tmp);
        }

        double determinant_C0C1 = cMatrix[0][0] * cMatrix[1][1] - cMatrix[1][0] * cMatrix[0][1];
        double determinant_C0X = cMatrix[0][0] * xMatrix[1] - cMatrix[0][1] * xMatrix[0];
        double determinant_XC1 = xMatrix[0] * cMatrix[1][1] - xMatrix[1] * cMatrix[0][1];

        if (determinant_C0C1 == 0)
            determinant_C0C1 = (cMatrix[0][0] * cMatrix[1][1]) * 10e-12f;

        double alphaL = determinant_XC1 / determinant_C0C1;
        double alphaR = determinant_C0X / determinant_C0C1;

        if (alphaL < 1e-6f || alphaR < 1e-6f) {
            double dist = last.distance(first) / 3f;

            CubicBezier curve = new CubicBezier();
            curve.setControlPoint(0, first);
            curve.setControlPoint(3, last);
            curve.setControlPoint(1, first.add(hat1.scale(dist)));
            curve.setControlPoint(2, last.add(hat2.scale(dist)));
            return curve;
        } else {
            CubicBezier curve = new CubicBezier();
            curve.setControlPoint(0, first);
            curve.setControlPoint(3, last);
            curve.setControlPoint(1, first.add(hat1.scale(alphaL)));
            curve.setControlPoint(2, last.add(hat2.scale(alphaR)));
            return curve;
        }
    }

    public static void reparameterize(List<ParameterizedPoint> points, CubicBezier curve) {
        for (int i = 0; i < points.size(); i++) {
            ParameterizedPoint p = points.get(i);
            p.setParameter(newtonRaphsonRootFind(curve, p));
        }
    }

    public static double newtonRaphsonRootFind(CubicBezier curve, ParameterizedPoint point) {
        QuadraticBezier firstDerivation = curve.getDerivation();
        LinearBezier secondDerivation = firstDerivation.getDerivation();

        Vec2 p = point.getPosition(); // expected
        Vec2 pCurve = curve.eval(point.getParameter()); // actual
        Vec2 pFirstDerivation = firstDerivation.eval(point.getParameter());
        Vec2 pSecondDerivation = secondDerivation.eval(point.getParameter());

        double numerator = (pCurve.getX() - p.getX() /* x error between expected and actual */) * pFirstDerivation.getX() +
                (pCurve.getY() - p.getY() /* y error between expected and actual */) * (pFirstDerivation.getY());
        double denominator = pFirstDerivation.lengthSquared() +
                (pCurve.getX() - p.getX() /* x error between expected and actual */) * pSecondDerivation.getX() +
                (pCurve.getY() - p.getY() /* y error between expected and actual */) * pSecondDerivation.getY();

        return point.getParameter() - (numerator / denominator);
    }

    public static Vec2 computeLeftTangent(List<ParameterizedPoint> points) {
        ParameterizedPoint p0 = points.get(0);
        ParameterizedPoint p1 = points.get(1);
        return p1.getPosition().sub(p0.getPosition()).normalize();
    }

    public static Vec2 computeRightTangent(List<ParameterizedPoint> points) {
        ParameterizedPoint p0 = points.get(points.size() - 1);
        ParameterizedPoint p1 = points.get(points.size() - 2);
        return p1.getPosition().sub(p0.getPosition()).normalize();
    }

    public static Vec2 computeCenterTangent(List<ParameterizedPoint> points, int center) {
        ParameterizedPoint c = points.get(center);
        Vec2 v1 = points.get(center - 1).getPosition().sub(c.getPosition());
        Vec2 v2 = c.getPosition().sub(points.get(center + 1).getPosition());
        return v1.add(v2).scale(0.5f).normalize();
    }

    /**
     * Try to guess parameter 't' for each point by computing the distance between the first point and
     * the current one through all points inbetween and divide it by the total distance between all points.
     */
    public static void chordLengthParameterize(List<ParameterizedPoint> points) {
        ParameterizedPoint prev = points.get(0);
        prev.setParameter(0);

        // Compute the distance to the first point through all previous points
        for (int i = 1; i < points.size(); i++) {
            ParameterizedPoint point = points.get(i);
            point.setParameter(prev.getParameter() + point.getPosition().distance(prev.getPosition()));
            prev = point;
        }

        // Divide distance by the total distance between all points
        for (int i = 1; i < points.size(); i++) {
            ParameterizedPoint point = points.get(i);
            point.setParameter(point.getParameter() / prev.getParameter());
        }
    }

    public static double computeMaxError(List<ParameterizedPoint> points, CubicBezier curve, Pointer<Integer> indexWithMaxError) {
        indexWithMaxError.set(points.size() / 2);
        double maxDist = 0;
        for (int i = 0; i < points.size(); i++) {
            ParameterizedPoint point = points.get(i);
            Vec2 p = curve.eval(point.getParameter());
            Vec2 v = p.sub(point.getPosition());
            double dist = v.lengthSquared();

            if (dist >= maxDist) {
                maxDist = dist;
                indexWithMaxError.set(i);
            }
        }
        return maxDist;
    }
}
