package io.inkable.notes.fitting;

import android.graphics.Path;

import java.util.ArrayList;
import java.util.List;

import io.inkable.notes.util.Vec2;

public abstract class BezierCurve {

    protected final Vec2[] mControlPoints;
    private int mDetail;
    private Path mPath;
    private boolean mUpdated;

    public BezierCurve(int count) {
        mControlPoints = new Vec2[count];
        mDetail = -1;
        mUpdated = true;
    }

    public Vec2[] getControlPoints() {
        return mControlPoints;
    }

    public Vec2 getControlPoint(int idx) {
        return mControlPoints[idx];
    }

    public void setControlPoint(int idx, Vec2 val) {
        mControlPoints[idx] = val;
        forceUpdate();
    }

    public int getDetail() {
        return mDetail;
    }

    public void setDetail(int detail) {
        mDetail = detail;
        forceUpdate();
    }

    public List<Vec2> calculatePoints(int detail) {
        List<Vec2> result = new ArrayList<>();

        double delta = 1.f / detail;
        for (double t = 0; t <= 1; t += delta)
            result.add(eval(t));

        result.add(mControlPoints[mControlPoints.length - 1]);
        return result;
    }

    public boolean isValid() {
        for (Vec2 c : mControlPoints) {
            if (Double.isNaN(c.getX()) || Double.isNaN(c.getY()))
                return false;
        }
        return true;
    }

    public Path getPath() {
        if (mPath == null || mUpdated) {
            mPath = new Path();
            mUpdated = false;

            int detail = mDetail;
            if (detail == -1)
                detail = (mControlPoints.length - 2) * 10;

            Vec2 first = mControlPoints[0];
            mPath.moveTo(first.getFloatX(), first.getFloatY());

            double delta = 1.f / detail;
            for (double t = delta; t <= 1; t += delta) {
                Vec2 point = eval(t);
                mPath.lineTo(point.getFloatX(), point.getFloatY());
            }

            Vec2 last = mControlPoints[mControlPoints.length - 1];
            mPath.lineTo(last.getFloatX(), last.getFloatY());
        }

        return mPath;
    }

    public void forceUpdate() {
        mUpdated = true;
    }

    public abstract Vec2 eval(double t);

    public abstract BezierCurve getDerivation();
}
