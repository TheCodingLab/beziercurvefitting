package io.inkable.notes.draw;

import io.inkable.notes.util.Vec2;

public class DataPoint {

    private Vec2 mPosition;
    private float mPressure;

    public DataPoint(Vec2 position, float pressure) {
        mPosition = position;
        mPressure = pressure;
    }

    public Vec2 getPosition() {
        return mPosition;
    }

    public float getPressure() {
        return mPressure;
    }
}
