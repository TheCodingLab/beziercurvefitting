package io.inkable.notes.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.os.Debug;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.Arrays;
import java.util.List;

import io.inkable.notes.fitting.CubicBezier;
import io.inkable.notes.util.Vec2;

public class DrawCanvas extends View {

    private PointRegister mPointRegister;
    private Vec2 mTranslate;
    private Vec2 mLast;

    public DrawCanvas(Context context) {
        super(context);
        mPointRegister = new PointRegister(20);
        mTranslate = new Vec2();
    }

    protected void handleTranslate(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mLast = new Vec2(event.getX(), event.getY());
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            Vec2 current = new Vec2(event.getX(), event.getY());
            Vec2 delta = current.sub(mLast);
            mLast = current;

            mTranslate = mTranslate.add(delta);
            invalidate();
        }
    }

    protected void handleDrawEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            mPointRegister.forceComputeBezierCurves();

        Vec2 position = new Vec2(event.getX(), event.getY()).sub(mTranslate);
        float pressure = event.getPressure();
        // TODO isButtonPressed?
        mPointRegister.push(new DataPoint(position, pressure));

        if (event.getAction() == MotionEvent.ACTION_UP)
            mPointRegister.forceComputeBezierCurves();

        invalidate();
    }

    protected void handleEraseEvent(MotionEvent event) {
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getPointerCount() == 1) {
            int type = event.getToolType(0);

            if (type == MotionEvent.TOOL_TYPE_FINGER) {
                handleTranslate(event);
            } else if (type == MotionEvent.TOOL_TYPE_STYLUS)
                handleDrawEvent(event);
            else if (type == MotionEvent.TOOL_TYPE_ERASER)
                handleEraseEvent(event);
        } else {
            // TODO zoom, rotation, etc
        }

        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        canvas.translate(mTranslate.getFloatX(), mTranslate.getFloatY());

        List<CubicBezier> curves = mPointRegister.getBezierCurves();
        for (CubicBezier curve : curves) {
            if (!curve.isValid())
                continue;

            Path path = curve.getPath();
            canvas.drawPath(path, DebugPaints.BEZIER_PAINT);

            /*Vec2[] controlPoints = curve.getControlPoints();
            for (Vec2 point : controlPoints)
                canvas.drawPoint(point.getFloatX(), point.getFloatY(), DebugPaints.CONTROL_POINT_PAINT);*/
        }

        DataPoint[] points = mPointRegister.getPoints();
        for (DataPoint point : points) {
            Vec2 p = point.getPosition();
            canvas.drawPoint(p.getFloatX(), p.getFloatY(), DebugPaints.DRAW_PAINT);
        }
    }
}
