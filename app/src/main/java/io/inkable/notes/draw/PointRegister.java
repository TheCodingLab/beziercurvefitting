package io.inkable.notes.draw;

import java.util.ArrayList;
import java.util.List;

import io.inkable.notes.fitting.CubicBezier;
import io.inkable.notes.fitting.CurveFitting;
import io.inkable.notes.util.Vec2;

public class PointRegister {

    private List<CubicBezier> mBezierCurves;
    private DataPoint[] mPoints;
    private int mCurrentIndex;

    public PointRegister(int capacity) {
        mBezierCurves = new ArrayList<>();
        mPoints = new DataPoint[capacity];
        mCurrentIndex = 0;
    }

    public List<CubicBezier> getBezierCurves() {
        return mBezierCurves;
    }

    public DataPoint[] getPointsRegister() {
        return mPoints;
    }

    public DataPoint[] getPoints() {
        DataPoint[] result = new DataPoint[mCurrentIndex];
        System.arraycopy(mPoints, 0, result, 0, mCurrentIndex);
        return result;
    }

    public int getCurrentIndex() {
        return mCurrentIndex;
    }

    public void push(DataPoint point) {
        if (mCurrentIndex >= mPoints.length)
            forceComputeBezierCurves();
        mPoints[mCurrentIndex++] = point;
    }

    public void forceComputeBezierCurves() {
        if (mCurrentIndex < 2)
            return;

        // TODO pressure

        List<Vec2> points = new ArrayList<>(mCurrentIndex);
        for (int i = 0; i < mCurrentIndex; i++)
            points.add(mPoints[i].getPosition());
        mCurrentIndex = 0;

        List<CubicBezier> fitted = CurveFitting.fitCurve(points, 10); // TODO error value
        mBezierCurves.addAll(fitted);
    }
}
