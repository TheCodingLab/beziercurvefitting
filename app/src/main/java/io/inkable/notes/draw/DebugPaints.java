package io.inkable.notes.draw;

import android.graphics.Color;
import android.graphics.Paint;

public class DebugPaints {

    public static final Paint BEZIER_PAINT;
    public static final Paint CONTROL_POINT_PAINT;
    public static final Paint DRAW_PAINT;

    static {
        BEZIER_PAINT = new Paint();
        BEZIER_PAINT.setColor(Color.BLACK);
        BEZIER_PAINT.setStyle(Paint.Style.STROKE);
        BEZIER_PAINT.setStrokeJoin(Paint.Join.ROUND);
        BEZIER_PAINT.setStrokeWidth(10f);

        CONTROL_POINT_PAINT = new Paint();
        CONTROL_POINT_PAINT.setColor(Color.RED);
        CONTROL_POINT_PAINT.setStyle(Paint.Style.STROKE);
        CONTROL_POINT_PAINT.setStrokeJoin(Paint.Join.ROUND);
        CONTROL_POINT_PAINT.setStrokeWidth(20f);

        DRAW_PAINT = new Paint();
        DRAW_PAINT.setColor(Color.MAGENTA);
        DRAW_PAINT.setStyle(Paint.Style.STROKE);
        DRAW_PAINT.setStrokeJoin(Paint.Join.ROUND);
        DRAW_PAINT.setStrokeWidth(10f);
    }
}
