package io.inkable.notes.util;

public class Pointer<T> {

    T mValue;

    public Pointer() {
    }

    public Pointer(T value) {
        mValue = value;
    }

    public T get() {
        return mValue;
    }

    public void set(T value) {
        mValue = value;
    }
}
