package io.inkable.notes.util;

public class Vec2 {

    private double x;
    private double y;

    public Vec2() {
    }

    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vec2(Vec2 other) {
        this.x = other.x;
        this.y = other.y;
    }

    public double getX() {
        return x;
    }

    public float getFloatX() {
        return (float) x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public float getFloatY() {
        return (float) y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void zero() {
        this.x = 0;
        this.y = 0;
    }

    public void set(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vec2 add(Vec2 o) {
        return new Vec2(this.x + o.x, this.y + o.y);
    }

    public Vec2 sub(Vec2 o) {
        return new Vec2(this.x - o.x, this.y - o.y);
    }

    public Vec2 scale(double scalar) {
        return new Vec2(this.x * scalar, this.y * scalar);
    }

    public double distanceSquared(Vec2 o) {
        double deltaX = this.x - o.x;
        double deltaY = this.y - o.y;
        return deltaX * deltaX + deltaY * deltaY;
    }

    public double distance(Vec2 o) {
        return Math.sqrt(distanceSquared(o));
    }

    public double lengthSquared() {
        return x * x + y * y;
    }

    public double length() {
        return Math.sqrt(x * x + y * y);
    }

    public Vec2 normalize() {
        return scale(1 / length());
    }

    public Vec2 negate() {
        return new Vec2(-this.x, -this.y);
    }

    public double dot(Vec2 o) {
        return this.x * o.x + this.y * o.y;
    }

    public static Vec2 interpolate(Vec2 a, Vec2 b, double t) {
        double deltaX = b.x - a.x;
        double deltaY = b.y - a.y;
        return new Vec2(a.x + deltaX * t, a.y + deltaY * t);
    }

    @Override
    public String toString() {
        return "Point[x: " + this.x + ", y: " + this.y + "]";
    }
}
