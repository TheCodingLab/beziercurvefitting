package io.inkable.notes;

import android.app.Activity;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.inkable.notes.draw.DrawCanvas;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DrawCanvas canvas = new DrawCanvas(this);
        setContentView(canvas);
    }
}
